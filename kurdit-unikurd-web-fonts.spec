%global fontname kurdit-unikurd-web

Name:          %{fontname}-fonts
Version:       20020502
Release:       23
Summary:       Kurdish font
License:       GPLv3
URL:           http://www.kurditgroup.org/node/1337
Source0:       https://www.kurditgroup.org/sites/default/files/unikurdweb_0.zip
Source1:       65-kurdit-unikurd-web.conf
Source2:       kurdit-unikurd-web.metainfo.xml

BuildArch:     noarch
BuildRequires: fontpackages-devel
Requires:      fontpackages-filesystem
Obsoletes:     unikurd-web-font < 20020502-2

%description
This package provides Kurdish font which is widely used and can
support Arabic,Persian and Latin scripts.

%prep
%autosetup -c unikurdweb

%build

%install
install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}
install -m 0755 -d %{buildroot}%{_fontconfig_confdir}
install -m 0755 -d %{buildroot}%{_fontconfig_templatedir}
install -m 0644 -p %{SOURCE1} %{buildroot}%{_fontconfig_templatedir}/65-kurdit-unikurd-web.conf
install -Dm 0644 -p %{SOURCE2} %{buildroot}%{_datadir}/appdata/kurdit-unikurd-web.metainfo.xml
ln -s %{_fontconfig_templatedir}/65-kurdit-unikurd-web.conf %{buildroot}%{_fontconfig_confdir}/65-kurdit-unikurd-web.conf

%_font_pkg -f 65-kurdit-unikurd-web.conf *.ttf
%license gpl.txt
%{_datadir}/appdata/kurdit-unikurd-web.metainfo.xml

%changelog
* Wed Jan 15 2020 lihao <lihao129@huawei.com> - 20020502-23
- Package Init
